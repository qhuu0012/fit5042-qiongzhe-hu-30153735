package ass.tutex.controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import ass.tutex.repository.AdminRepository;
import ass.tutex.repository.CustomerRepository;
import ass.tutex.repository.entities.Customer;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@Named(value = "customerApplication")
@ApplicationScoped

public class CustomerApplication {

	@ManagedProperty(value="#{customerRepo}") 
    CustomerRepository customerRepo;
	
	private ArrayList<Customer> customers;
	private boolean showForm = true;
	
	public boolean isShowForm() {
        return showForm;
    }
	
	public CustomerApplication() throws Exception {
		customers = new ArrayList<>();
	        
	        //instantiate propertyManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerRepo = (CustomerRepository) FacesContext.getCurrentInstance().getApplication()
	        .getELResolver().getValue(elContext, null, "customerRepo");
	        
	        //get properties from db 
	        updateCustomerList();
	    }
	
	public ArrayList<Customer> getCustomers() {
        return customers;
    }
	
	private void setCustomers(ArrayList<Customer> newCustomers) {
        this.customers = newCustomers;
    }
	
	
	public void updateCustomerList() {
        if (customers != null && customers.size() > 0)
        {
            
        }
        else
        {
        	customers.clear();

            for (ass.tutex.repository.entities.Customer customer : customerRepo.getAllCustomers())
            {
            	customers.add(customer);
            }

            setCustomers(customers);
        }
    }
	
	public Customer searchCustomerById(int customerId)
    {
    	customers.clear();
        
    	customers.add(customerRepo.searchCustomerId(customerId));
    	
    	return customers.get(0);
    }
	
	
	 public void searchAll()
	    {
	        customers.clear();
	        
	        for (ass.tutex.repository.entities.Customer customer : customerRepo.getAllCustomers())
	        {
	        	customers.add(customer);
	        }
	        
	        setCustomers(customers);
	    }
	
}
