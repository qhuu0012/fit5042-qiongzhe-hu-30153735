package ass.tutex.repository;

import java.util.List;

import javax.ejb.Remote;

import ass.tutex.repository.entities.Staff;

@Remote
public interface AdminRepository {
	
	public List<Staff> getAllStaffs();
	
	public Staff searchStaffsById(int staffId);
}

