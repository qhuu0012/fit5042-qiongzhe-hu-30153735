package ass.tutex.repository.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@NamedQueries({ @NamedQuery(name = Contact.GET_ALL_CONTACTS, query = "select t.id, t.contactName from Contact t"),
		@NamedQuery(name = Contact.GET_LAST_CONTACT, query = "select t.id, t.contactName from Contact t order by t.id desc"), })

@XmlRootElement
public class Contact implements Serializable {

	public static final String GET_ALL_CONTACTS = "getAllContacts";
	public static final String GET_LAST_CONTACT = "getLastContact";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String contactName;
	private String email;
	private String mobile;
	private String gender;
	private String description;

	@JsonBackReference
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@JoinColumn(name = "cus_id", nullable = false)
	private Customer customer;

	public Contact() {

	}

	public Contact(int id, String contactName, String email, String mobile, String gender, String description, Customer customer) {
		super();
		this.id = id;
		this.contactName = contactName;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.description = description;
		this.customer = customer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	 
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Contact other = (Contact) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}

}
