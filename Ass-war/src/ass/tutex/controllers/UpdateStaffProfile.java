package ass.tutex.controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ass.tutex.repository.AppUserRepository;
import ass.tutex.repository.entities.Staff;

@Named(value = "updateStaffProfile")
@RequestScoped
public class UpdateStaffProfile {

    @EJB
    private AppUserRepository userRepo;
//    id will not be changed during the update process
    private static int id;

    @NotNull(message = "First name can not be empty!!!")
    private String firstName;

    @NotNull(message = "Last name can not be empty!!!")
    private String lastName;

    @NotNull(message = "Email can not be empty")
    @Pattern(regexp = "[\\w\\.-]*[a-zA-Z0-9_]@[\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]", message = "Email invalid!!!")
    private String email;

    @NotNull(message = "Password can not be empty!!!")
    @Size(min = 3, max = 3, message = "Password must have 3 digits")
    private String password;

    @NotNull(message = "Phone number can not be empty")
    @Pattern(regexp = "\\d{10}", message = "Phone number invalid!!!")
    private String phone;

    @NotNull(message = "Birthday can not be empty!!!")
    private Date dob;

    @NotNull(message = "StaffNo can not be empty!!!")
    private int staffNo;
    
    private String message;

    /**
     * Creates a new instance of UpdatestaffProfile
     */
    public UpdateStaffProfile() {
    }
    
    public int getStaffNo() {
		return staffNo;
	}

	public void setStaffNo(int staffNo) {
		this.staffNo = staffNo;
	}


	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String sha256Password(String password) {
        MessageDigest md;
        StringBuilder sb = new StringBuilder();
        try {

            md = MessageDigest.getInstance("SHA-256");
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AddUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }

    public String updateStaffProfile(int s_id) {

        Staff currentStaff = userRepo.selectStaffById(s_id);
        currentStaff.setFirstName(firstName);
        currentStaff.setLastName(lastName);
        currentStaff.setEmail(email);
        currentStaff.setPassword(sha256Password(password));
        currentStaff.setPhone(phone);
        currentStaff.setDob(dob);
        currentStaff.setStaffNo(staffNo);

        userRepo.updateStaff(currentStaff);

        message = "Update successfully!!!";

        return "staffProfile";

    }

    public void obtainCurrentstaff() {
        Principal authUser = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
        Staff currentStaff = userRepo.selectStaffByEmail(authUser.getName());
        id = currentStaff.getId();
        firstName = currentStaff.getFirstName();
        lastName = currentStaff.getLastName();
        email = currentStaff.getEmail();
        password = currentStaff.getPassword();
        phone=currentStaff.getPhone();
        dob = currentStaff.getDob();
        staffNo = currentStaff.getStaffNo();

    }

}
