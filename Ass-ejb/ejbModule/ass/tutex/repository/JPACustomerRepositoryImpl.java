package ass.tutex.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ass.tutex.repository.entities.AppUser;
import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Customer;
import ass.tutex.repository.entities.Staff;

@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository {
	@PersistenceContext(unitName = "Ass-ejb")
	private EntityManager entityManager;

	@Override
	public void addCustomer(Customer customer){
		List<Customer> customers =  entityManager.createNamedQuery(Customer.GET_ALL_CUSTOMERS).getResultList(); 
    	customer.setId(customers.get(0).getId() + 1);
        entityManager.persist(customer);
		
	}

	@Override
	public Customer searchCustomerId(int id){
		Customer customer = entityManager.find(Customer.class, id);
        return customer;
	}

	@Override
	public void removeCustomer(int customerId){
		Customer customer = this.searchCustomerId(customerId);

        if (customer!= null) {
            entityManager.remove(customer);
        }
		
	}

	@Override
	public void editCustomer(Customer customer) {
		try {
            entityManager.merge(customer);
        } catch (Exception ex) {
            
        }
		
	}

	@Override
	public List<Customer> getAllCustomers() {
		return entityManager.createNamedQuery(Customer.GET_ALL_CUSTOMERS).getResultList();
	}

	
	@Override
	public List<Customer> searchCustomerByStaff(Staff staff){
		staff = entityManager.find(Staff.class, staff.getId());
        staff.getOwnCustomers().size();
        entityManager.refresh(staff);

        return staff.getOwnCustomers();
	}


	@Override
	public List<Contact> getAllContactByCustomer(Customer customer){
		customer = entityManager.find(Customer.class, customer.getId());
        customer.getOwnContacts().size();
        entityManager.refresh(customer);

        return customer.getOwnContacts();
	}

}
