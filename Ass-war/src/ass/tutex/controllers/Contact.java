package ass.tutex.controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import ass.tutex.repository.entities.Customer;
import ass.tutex.repository.entities.Industry;


@RequestScoped
@Named(value = "contact")
public class Contact implements Serializable {
	private int contactId;
	private String contactName;
	
	
	private String email;
	private String mobile;
	
	private String gender;
	private String description;

	
	private Customer customer;
	
	private int customerId;

	private String address;
	private Industry industry;
	private int noOfStaff;
	private String businessName;
    private String ceo;
    private String officePhone;
    private String customerEmail;
    
    private String industryType;
    private String industryDesc;
    
    private Set<ass.tutex.repository.entities.Contact> contacts; 

	public Contact() {

	}

	public Contact(int contactId, String contactName, String email, String mobile, String gender, String description, Customer customer) {
		super();
		this.contactId = contactId;
		this.contactName = contactName;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.description = description;
		this.customer = customer;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	 
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public int getNoOfStaff() {
		return noOfStaff;
	}

	public void setNoOfStaff(int noOfStaff) {
		this.noOfStaff = noOfStaff;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public Set<ass.tutex.repository.entities.Contact> getContacts() {
		return contacts;
	}


	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getIndustryDesc() {
		return industryDesc;
	}

	public void setIndustryDesc(String industryDesc) {
		this.industryDesc = industryDesc;
	}


	public void setContacts(Set<ass.tutex.repository.entities.Contact> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", contactName=" + contactName + ", email=" + email + ", mobile="
				+ mobile + ", gender=" + gender + ", description=" + description + ", customer=" + customer
				+ ", customerId=" + customerId + ", address=" + address + ", industry=" + industry + ", noOfStaff="
				+ noOfStaff + ", businessName=" + businessName + ", ceo=" + ceo + ", officePhone=" + officePhone
				+ ", customerEmail=" + customerEmail + ", industryType=" + industryType + ", industryDesc="
				+ industryDesc + ", contacts=" + contacts + "]";
	}
	
	
	
	

}
