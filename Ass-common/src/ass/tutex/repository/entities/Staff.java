package ass.tutex.repository.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Staff extends AppUser implements Serializable{

	private int staffNo;
	
		@Temporal(javax.persistence.TemporalType.DATE)
	    private Date dob;
	
		//@OneToMany(mappedBy = "staff",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
	    //private List<Customer> ownCustomers;

		//@OneToMany(mappedBy = "staff",fetch = FetchType.LAZY,cascade ={ CascadeType.REMOVE})
	   // private List<Contact> ownContacts;
		
		//@OneToMany(mappedBy = "staff",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
	    //private List<Deal> ownDeals;
		
		@ManyToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
	    @JoinTable(name = "staff_customer", joinColumns = @JoinColumn(name = "sta_id_fk"), inverseJoinColumns = @JoinColumn(name = "cus_id_fk"))
	    private List<Customer> ownCustomers;
	
    public Staff() {
    }
    
    
	public Staff(int id, int powerLevel, String firstName, String lastName, String email, String password, String phone,
			int staffNo, Date dob, List<Customer> ownCustomers) {
		super(id, powerLevel, firstName, lastName, email, password, phone);
		this.staffNo = staffNo;
		this.dob = dob;
		this.ownCustomers = ownCustomers;
	}




	public List<Customer> getOwnCustomers() {
		return ownCustomers;
	}


	public void setOwnCustomerss(List<Customer> ownCustomers) {
		this.ownCustomers = ownCustomers;
	}


	public int getStaffNo() {
		return staffNo;
	}

	public void setStaffNo(int staffNo) {
		this.staffNo = staffNo;
	}



	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

}

