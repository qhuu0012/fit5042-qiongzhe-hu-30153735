package ass.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ass.tutex.repository.ContactRepository;
import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Customer;

import javax.faces.bean.ManagedProperty;


@RequestScoped
@Named("AddCustomerContact")
public class AddContact {
	@ManagedProperty(value="#{contactRepo}") 
	ContactRepository contactRepo;
    
    private boolean showForm = true;

    private Contact contact;
    
    
    private Customer customer;
    
    private int customerId;
    
    ContactApplication app;
    
    CustomerApplication custApp;
    
    public void setCustomerContact(Contact contact){
        this.contact = contact;
    }
    
    public Contact getCContact(){
        return contact;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public AddContact() 
    {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app  = (ContactApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "contactApplication");
        
        custApp  = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");
        
        //instantiate customerContactManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactRepo = (ContactRepository) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerContactManagedBean");
         
        
        //instantiate customerContact
        contact = new Contact();
        
        //instantiate customerId and customer
        customerId = getCustomerId();
        //customerId = Integer.valueOf(
		//		FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerId"));
        
       
    }

    public void addContactInfor() {
        //this is the local property, not the entity
       try{
    	   
    	   setCustomer(custApp.searchCustomerById(customerId));
    	   contact.setCustomer(getCustomer());
            //add this property to db via EJB
    	   contactRepo.addContact(contact);

            //refresh the list in PropertyApplication bean
            app.searchAll();
            //updatePropertyListInPropertyApplicationBean();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer Contact has been added succesfully"));
            //refresh the property list in propertyApplication bean
            
            //contactRepo.addCustomer(localCustomer);
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	
}