package ass.tutex.controllers;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import ass.tutex.repository.AppUserRepository;
import ass.tutex.repository.entities.AppUser;

@Named(value = "getAllUsers")
@RequestScoped
public class GetAllUsers {

    @EJB
    private AppUserRepository userRepo;

    /**
     * Creates a new instance of GetAllUsers
     */
    public GetAllUsers(){
    	
    }

    public List<AppUser> getAllUsers() {
        return userRepo.getAllUsers();

    }
    
}