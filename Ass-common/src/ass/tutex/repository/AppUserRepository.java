package ass.tutex.repository;

import java.util.List;

import javax.ejb.Remote;

import ass.tutex.repository.entities.Admin;
import ass.tutex.repository.entities.AppUser;
import ass.tutex.repository.entities.Staff;

@Remote
public interface AppUserRepository {
	
	//public void addUser(AppUser appUser) throws Exception;
	
	//public List<AppUser> getAllUsers() throws Exception;
	   
    //public void updateUser(AppUser appUser) throws Exception;
    
    //public void deleteUser(int userId) throws Exception;
	
	//public AppUser searchUserById(int id) throws Exception;
	
	
	//public List<Customer> searchCustomerByStaff(Staff staff) throws Exception;
	
	
	public AppUser validateUser(String email, String pwd);

    public List<AppUser> getAllUsers();

    public void deleteUserById(int id);

    public Staff selectStaffById(int id);

    public Staff selectStaffByEmail(String email);

    public Admin selectAdminById(int id);

    public void updateStaff(Staff s);

    public void updateAdmin(Admin a);

    public void addStaff(Staff s);

    public void addAdmin(Admin a);
   
}

