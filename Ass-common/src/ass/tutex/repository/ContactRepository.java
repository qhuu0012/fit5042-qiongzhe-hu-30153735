package ass.tutex.repository;

import java.util.List;

import javax.ejb.Remote;

import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Customer;
import ass.tutex.repository.entities.Staff;

@Remote
public interface ContactRepository {

    public static void addContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

    public static List<Contact> getAllContacts() {
		// TODO Auto-generated method stub
		return null;
	}
    
    public void editContact(Contact contact);
    
    public static void removeContact(int contactId) {
		// TODO Auto-generated method stub
		
	}
    
    public static Contact searchContactId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

  
    public List<Contact> searchContactByCustomer(int CustomerId);
    
}
