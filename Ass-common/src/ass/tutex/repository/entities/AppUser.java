package ass.tutex.repository.entities;


/*
 * Qiongzhe Hu
 */
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NamedQueries({
    @NamedQuery(name = AppUser.GET_ALL_USERS, query = "select u from AppUser u")
    ,
  @NamedQuery(name = AppUser.GET_LAST_USER, query = "select u from AppUser u order by u.id desc"),})
public class AppUser implements Serializable {

    public static final String GET_ALL_USERS = "getAllUsers";
    public static final String GET_LAST_USER = "getLastUsers";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    
    private int powerLevel;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    
    //@OneToMany(mappedBy = "appUser",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
    //private List<Customer> ownCustomers;

    public AppUser() {
    }

    
    public AppUser(int id, int powerLevel, String firstName, String lastName, String email, String password,
			String phone) {
		super();
		this.id = id;
		this.powerLevel = powerLevel;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phone = phone;
		//this.ownCustomers = ownCustomers;
	}


	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPowerLevel() {
        return powerLevel;
    }

    public void setPowerLevel(int powerLevel) {
        this.powerLevel = powerLevel;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
    
    

}