package ass.tutex.controllers;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ass.tutex.repository.entities.Contact;

@RequestScoped
@Named("searchContact")
public class SearchContact {
	private boolean showForm = true;
	private Contact contact;

	ContactApplication app;

	private int searchByInt;
	
	private int customerId;
	
	public int getCustomerId() {

        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

	public ContactApplication getApp() {
		return app;
	}

	public void setApp(ContactApplication app) {
		this.app = app;
	}


	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}

	public SearchContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (ContactApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "contactApplication");
        
        app.updateContactList();
    }

	/**
	 * Normally each page should have a backing bean but you can actually do it any
	 * how you want.
	 *
	 * @param customer Index
	 */
	public void searchContactById(int customerIndex) {
		try {
			// search this customer then refresh the list in PropertyApplication bean
			app.searchContactById(customerIndex);
		} catch (Exception ex) {

		}
	}

	public boolean isShowForm() {
	        return showForm;
	    }
	
	public void searchContactByCustomerId(int customerId) {
        try {
        	int p = customerId;
            //search all customerContacts by customerId from db via EJB 
            app.searchContactByCustomerId(customerId);
        } catch (Exception ex) {

        }
        showForm = true;
    }

	public void searchAll() {
		try {
			// return all properties from db via EJB
			app.searchAll();
		} catch (Exception ex) {

		}
	}

}
