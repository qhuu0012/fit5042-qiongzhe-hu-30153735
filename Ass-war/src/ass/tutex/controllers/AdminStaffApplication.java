package ass.tutex.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ass.tutex.repository.AdminRepository;
import ass.tutex.repository.entities.Staff;

@Named(value = "adminStaffApplication")
@ApplicationScoped

public class AdminStaffApplication {

	@ManagedProperty(value="#{adminRepo}") 
    AdminRepository adminRepo;
	
	private ArrayList<Staff> staffs;
	private boolean showForm = true;
	public boolean isShowForm() {
        return showForm;
    }
	// Add some property data from db to app 
    public AdminStaffApplication() throws Exception {       
        staffs = new ArrayList<>();
        
        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        adminRepo = (AdminRepository) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "adminManagedBean");
        
        //get properties from db 
        updateStaffList();
    }

    public ArrayList<Staff> getStaffs() {
		return staffs;
	}
	public void setStaffs(ArrayList<Staff> newStaffs) {
		this.staffs =  newStaffs;
	}

	public void updateStaffList() {
		if(staffs != null && staffs.size() > 0) {
			
		}else {
			staffs.clear();
			for(ass.tutex.repository.entities.Staff staff : adminRepo.getAllStaffs()) {
				staffs.add(staff);
			}
			setStaffs(staffs);
		}
	}
	 

	public void searchStaffById(int staff_id)
	{
		staffs.clear();
		staffs.add(adminRepo.searchStaffsById(staff_id));
	}
	
	public void searchAllStaff() {
		staffs.clear();
		for(ass.tutex.repository.entities.Staff staff : adminRepo.getAllStaffs()) {
			staffs.add(staff);
		}
		setStaffs(staffs);
		
	}
}
