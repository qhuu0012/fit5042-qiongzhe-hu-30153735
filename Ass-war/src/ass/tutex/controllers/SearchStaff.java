package ass.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ass.tutex.repository.entities.Staff;

@RequestScoped
@Named("searchstaff")
public class SearchStaff {
	private Staff staff;
    
    AdminStaffApplication adminStaffApplication;
    private boolean showForm = true;
    private int searchByInt;

    public AdminStaffApplication getApp() {
        return adminStaffApplication;
    }

    public void setApp(AdminStaffApplication adminStaffApplication) {
        this.adminStaffApplication = adminStaffApplication;
    }
    
    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    public void setstaff(Staff staff){
        this.staff = staff;
    }
    
    public Staff getstaff(){
        return staff;
    }
    
    public SearchStaff() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        adminStaffApplication = (AdminStaffApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "adminStaffApplication");
        
        adminStaffApplication.updateStaffList();
    }
    public void searchstaffById(int staff_id) 
    {
       try
       {
            //search this staff then refresh the list in staffApplication bean
    	   adminStaffApplication.searchStaffById(staff_id);

       }
       catch (Exception ex)
       {
    	   showForm = true;
       }
    }
    
    public void searchAllstaff() 
    {
       try
       {
            //return all staffs from db via EJB
    	   adminStaffApplication.searchAllStaff();
       }
       catch (Exception ex)
       {
    	   showForm = true;
       }
    }
    
    public void ss() {
    	
    }
}
