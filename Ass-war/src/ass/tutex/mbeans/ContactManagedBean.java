package ass.tutex.mbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import ass.tutex.repository.ContactRepository;
import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Customer;
import ass.tutex.repository.entities.Industry;

@ManagedBean(name = "contactManagedBean")
@SessionScoped

public class ContactManagedBean implements Serializable{
	@EJB
	ContactRepository contactRepository;
	
	private int selectedId;

    public int getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(int selectedId) {
        this.selectedId = selectedId;
    }

	/**
     * Creates a new instance of ContactManagedBean
     */
	 public ContactManagedBean() {
	    }

	public List<Contact> getAllContacts() {
		try {
			List<Contact> Contacts = ContactRepository.getAllContacts();
			return Contacts;
		} catch (Exception ex) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public void addContact(Contact Contact) {
		try {
			ContactRepository.addContact(Contact);
		} catch (Exception ex) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Search a Contact by Id
	 */
	public Contact searchContactById(int id) {
		try {
			return ContactRepository.searchContactId(id);
		} catch (Exception ex) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	public List<Contact> searchContactByCustomerId(int customerId) {
		try {
            //retrieve contact person by id
            for (Customer customer : ContactRepository.getAllCustomers()) {
                if (customer.getCustomerId() == customerId) {
                    return ContactRepository.searchContactByCustomer(customer);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

	
	public List<Customer> getAllCustomers() throws Exception {
		try {
			return ContactRepository.getAllCustomers();
		} catch (Exception ex) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	public void removeContact(int ContactId) {
		try {
			ContactRepository.removeContact(ContactId);
		} catch (Exception ex) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void editContact(Contact Contact) {
		try {
			
			int id = Contact.getCustomer().getCustomerId();
			Customer customer = Contact.getCustomer();
			customer.setCustomerId(id);
			Contact.setCustomer(customer);
			
			ContactRepository.editContact(Contact);

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Contact has been updated succesfully"));
		} catch (Exception ex) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void addContact(fit5042.assignm.controllers.Contact localContact) {
        //convert this newProperty which is the local property to entity property
		Contact Contact = convertContactToEntity(localContact);

        try {
        	ContactRepository.addContact(Contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	private Contact convertContactToEntity(fit5042.assignm.controllers.Contact localContact) {
		Contact Contact = new Contact(); //entity
        int customerId = localContact.getCustomerId();
        String address = localContact.getAddress();
        String companyName = localContact.getCompanyName();
        int officephone = localContact.getOfficephone();
        String registerDate = localContact.getRegisterDate();
        String aBN = localContact.getaBN();
        String email = localContact.getEmail();
        String CEO = localContact.getCEO();
        
        String industryType = localContact.getIndustryType();
        String industryDesc = localContact.getIndustryDesc();
        
        Industry industry = new Industry(industryType,industryDesc);
        
        Customer customer = new Customer(customerId, address , officephone,companyName,registerDate, aBN,email, CEO, industry, getAllContacts());
        
                    
        Contact.setContactId(localContact.getContactId());
        Contact.setContactName(localContact.getContactName());
        Contact.setContactPhone(localContact.getContactPhone());
        Contact.setContactEmail(localContact.getContactEmail());
        Contact.setPosition(localContact.getPosition());
        Contact.setGender(localContact.getGender());
        
        if (customer.getCustomerId() == 0)
            customer = null;
        Contact.setCustomer(customer);
        return Contact;
        
        
    }

}
