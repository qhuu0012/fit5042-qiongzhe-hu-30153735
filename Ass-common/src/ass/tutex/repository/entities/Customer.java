package ass.tutex.repository.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@NamedQueries({
	@NamedQuery(name = Customer.GET_ALL_CUSTOMERS, query = "select c from Customer c"),
	@NamedQuery(name = Customer.GET_LAST_CUSTOMER, query = "select c from Customer c order by c.id desc"),})
@XmlRootElement
public class Customer implements Serializable {
    
	public static final String GET_ALL_CUSTOMERS = "getAllCustomers";
    public static final String GET_LAST_CUSTOMER = "getLastCustomer";
    
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

	private String address;
	private Industry industry;
	private int noOfStaff;
	private String businessName;
    private String ceo;
    private String officePhone;
    private String email;

    @JsonBackReference
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "staff_customer", joinColumns = @JoinColumn(name = "cus_id_fk"), inverseJoinColumns = @JoinColumn(name = "sta_id_fk"))
    private List<Staff> relatedStaffs;
    
    //@JsonBackReference
	//@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST }, fetch = FetchType.EAGER)
	//@JoinColumn(name = "sta_id", nullable = false)
	//private Staff staff;
    
    @OneToMany(mappedBy = "customer",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
    private List<Contact> ownContacts;
    
    //@OneToMany(mappedBy = "customer",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
    //private List<Deal> ownDeals;
    
    
    //@ManyToMany(mappedBy = "customer",fetch = FetchType.EAGER,cascade = { CascadeType.REMOVE})
    //@JoinTable(name = "staff_customer", joinColumns = @JoinColumn(name = "cus_id_fk"), inverseJoinColumns = @JoinColumn(name = "sta_id_fk"))
    
    
    public Customer(){
    	
    }
    

	public Customer(int id, String address, Industry industry, int noOfStaff, String businessName, String ceo,
			String officePhone, String email, List<Contact> ownContacts) {
		super();
		this.id = id;
		this.address = address;
		this.industry = industry;
		this.noOfStaff = noOfStaff;
		this.businessName = businessName;
		this.ceo = ceo;
		this.officePhone = officePhone;
		this.email = email;
		this.ownContacts = ownContacts;
		//this.ownDeals = ownDeals;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public int getNoOfStaff() {
		return noOfStaff;
	}

	public void setNoOfStaff(int noOfStaff) {
		this.noOfStaff = noOfStaff;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Contact> getOwnContacts() {
		return ownContacts;
	}

	public void setOwnContacts(List<Contact> ownContacts) {
		this.ownContacts = ownContacts;
	}
}
