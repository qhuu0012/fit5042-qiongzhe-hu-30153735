package ass.tutex.repository;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import ass.tutex.repository.ContactRepository;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;

import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Customer;
import ass.tutex.repository.entities.Staff;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
public class JPAContactRepositoryImpl implements ContactRepository{
	@PersistenceContext(unitName = "Ass-ejb")
	private EntityManager entityManager;

	@Override
	public void addContact(Contact contact) {
		List<Contact> contacts =  entityManager.createNamedQuery(Contact.GET_ALL_CONTACTS).getResultList(); 
    	contact.setId(contacts.get(0).getId() + 1);
        Customer customer = contact.getCustomer();
        customer.getOwnContacts().add(contact);
    	entityManager.merge(customer);
	}

	@Override
	public Contact searchContactId(int id){
		Contact contact = entityManager.find(Contact.class, id);
		return contact;
	}

	@Override
	public List<Contact> getAllContacts(){
		return entityManager.createNamedQuery(Contact.GET_ALL_CONTACTS).getResultList();
	}

	@Override
	public List<Contact> searchContactByCustomer(int customerId){
		Customer customer = entityManager.find(Customer.class, customerId);
	    customer.getOwnContacts().size();
	    entityManager.refresh(customer);
	    return customer.getOwnContacts();
		
	}
	
	/*@Override
	public List<Contact> searchContactByCustomer(int customerId){
		customer = entityManager.find(Customer.class, customer.getId());
	    customer.getOwnContacts().size();
	    entityManager.refresh(customer);
	    return customer.getOwnContacts();
	}*/


	@Override
	public void removeContact(int contactId) {
		Contact contact = this.searchContactId(contactId);

        if (contact != null) {
            entityManager.remove(contact);
        }
		
	}

	@Override
	public void editContact(Contact contact)  {
		try {
            entityManager.merge(contact);
        } catch (Exception ex) {
            
        }
		
	}

	
}
