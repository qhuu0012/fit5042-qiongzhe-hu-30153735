package ass.tutex.controllers;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import ass.tutex.repository.AppUserRepository;
import ass.tutex.repository.entities.Admin;
import ass.tutex.repository.entities.Staff;

@Named(value = "addUser")
@RequestScoped
public class AddUser {

    @EJB
    private AppUserRepository userRepo;

    private Admin admin;
    private Staff staff;

    private int powerLevel;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;

    private static boolean isInfoValid = false;

    private String conPassword;
    private String invalidPwdMsg;

    /**
     * Creates a new instance of AddUser
     */
    public AddUser() {
    }

	public AppUserRepository getUserRepo() {
		return userRepo;
	}


	public void setUserRepo(AppUserRepository userRepo) {
		this.userRepo = userRepo;
	}


	public Admin getAdmin() {
		return admin;
	}


	public void setAdmin(Admin admin) {
		this.admin = admin;
	}


	public Staff getStaff() {
		return staff;
	}


	public void setStaff(Staff staff) {
		this.staff = staff;
	}


	public int getPowerLevel() {
		return powerLevel;
	}


	public void setPowerLevel(int powerLevel) {
		this.powerLevel = powerLevel;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public static boolean isInfoValid() {
		return isInfoValid;
	}


	public static void setInfoValid(boolean isInfoValid) {
		AddUser.isInfoValid = isInfoValid;
	}


	public String getConPassword() {
		return conPassword;
	}


	public void setConPassword(String conPassword) {
		this.conPassword = conPassword;
	}


	public String getInvalidPwdMsg() {
		return invalidPwdMsg;
	}


	public void setInvalidPwdMsg(String invalidPwdMsg) {
		this.invalidPwdMsg = invalidPwdMsg;
	}
	
	@PostConstruct
    public void init() {
        staff = new Staff();
        admin = new Admin();

    }
}