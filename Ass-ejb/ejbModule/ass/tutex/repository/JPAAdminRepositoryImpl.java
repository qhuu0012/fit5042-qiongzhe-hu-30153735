package ass.tutex.repository;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ass.tutex.repository.entities.AppUser;
import ass.tutex.repository.entities.Staff;

@Stateless
public class JPAAdminRepositoryImpl implements AdminRepository{

	@PersistenceContext(unitName = "Ass-ejb")
    private EntityManager entityManager;
	
	@Override
	public List<Staff> getAllStaffs() {
		List<AppUser> toDo = new ArrayList<AppUser>();
		toDo = entityManager.createNamedQuery(AppUser.GET_ALL_USERS).getResultList();
		List<Staff> staffsList = new ArrayList<Staff>();
		 for (int i = 0; i < toDo.size(); i++) {
			 if(toDo.get(i) instanceof Staff) {
				 staffsList.add((Staff) toDo.get(i));
			 }
		 }
		return staffsList;
	}

	@Override
	public Staff searchStaffsById(int staffId) {
		return entityManager.find(Staff.class, staffId);
	}

}
