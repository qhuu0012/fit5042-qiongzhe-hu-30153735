package ass.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ass.tutex.repository.ContactRepository;
import ass.tutex.repository.entities.Contact;

import javax.faces.bean.ManagedProperty;

@RequestScoped
@Named("removeContact")
public class RemoveContact {
	@ManagedProperty(value = "#{contactRepo}")
	ContactRepository contactRepo;

	private boolean showForm = true;
	private Contact contact;
	ContactApplication app;

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public RemoveContact() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();

	        app = (ContactApplication) FacesContext.getCurrentInstance()
	                        .getApplication()
	                        .getELResolver()
	                        .getValue(context, null, "contactApplication");
	        
	        app.updateContactList();
	        
	        //instantiate propertyManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        contactRepo = (ContactRepository) FacesContext.getCurrentInstance().getApplication()
	        .getELResolver().getValue(elContext, null, "contactRepo");
	    }

	/**
	 * @param property Id
	 */
	public void removeContact(int contactId) {
		try {
			// remove this property from db via EJB
			contactRepo.removeContact(contactId);

			// refresh the list in PropertyApplication bean
			app.searchAll();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Contact has been deleted succesfully"));
		} catch (Exception ex) {

		}
		showForm = true;

	}

}