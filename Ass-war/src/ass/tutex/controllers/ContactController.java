package ass.tutex.controllers;

import java.io.Serializable;

import javax.el.ELContext;
import javax.inject.Named;

import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


@Named(value = "contactController")
@RequestScoped
public class ContactController {

	private int contactIndex; //this customerIndex is the index

    public int getcontactIndex() {
        return contactIndex;
    }

    public void setcontactIndex(int contactIndex) {
        this.contactIndex = contactIndex;
    }
    private ass.tutex.repository.entities.Contact contact;

    public ContactController() {
        // Assign customer identifier via GET param 
        //this customerIndex is the index, don't confuse with the customer Id
    	contactIndex = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactIndex"));
        // Assign customer based on the id provided 
        contact = getContact();
    }

    public ass.tutex.repository.entities.Contact getContact() {
        if (contact == null) {
            // Get application context bean CustomerApplication 
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            ContactApplication app
                    = (ContactApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "contactApplication");
            // -1 to customerId since we +1 in JSF (to always have positive customer index!) 
            return app.getContacts().get(--contactIndex); //this customer index is the index, don't confuse with the Customer Id
        }
       
        return contact;
    }

}
