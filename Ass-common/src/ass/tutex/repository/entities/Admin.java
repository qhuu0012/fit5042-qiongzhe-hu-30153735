package ass.tutex.repository.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
public class Admin extends AppUser implements Serializable {
	
	private int adminNo;

    public Admin() {
    }

    
	

	public Admin(int id, int powerLevel, String firstName, String lastName, String email, String password, String phone,
			int adminNo) {
		super(id, powerLevel, firstName, lastName, email, password, phone);
		this.adminNo = adminNo;
	}




	public int getAdminNo() {
		return adminNo;
	}

	public void setAdminNo(int adminNo) {
		this.adminNo = adminNo;
	}

}

