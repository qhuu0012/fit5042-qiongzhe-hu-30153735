  package ass.tutex.repository;

import java.util.List;
import java.util.function.Predicate;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ass.tutex.repository.entities.Admin;
import ass.tutex.repository.entities.AppUser;
import ass.tutex.repository.entities.Staff;

@Stateless
public class JPAAppUserRepositoryImpl implements AppUserRepository{

	@PersistenceContext(unitName = "Ass-ejb")
    private EntityManager entityManager;
	
	/*@Override
	public void addUser(AppUser appUser) throws Exception {
		List<AppUser> appUsers =  entityManager.createNamedQuery(AppUser.GET_ALL_USERS).getResultList(); 
        appUser.setId(appUsers.get(0).getId() + 1);
        entityManager.persist(appUser);
		
	}

	@Override
	public void deleteUser(int userId) throws Exception {
		AppUser appUser = this.searchUserById(userId);

        if (appUser != null) {
            entityManager.remove(appUser);
        }
		
	}

	@Override
	public void updateUser(AppUser appUser) throws Exception {
		try {
            entityManager.merge(appUser);
        } catch (Exception ex) {
            
        }
		
	}

	@Override
	public List<AppUser> getAllUsers() throws Exception {
		return entityManager.createNamedQuery(AppUser.GET_ALL_USERS).getResultList();
	}

	@Override
	public AppUser searchUserById(int id) throws Exception {
		AppUser appuser = entityManager.find(AppUser.class, id);
        return appuser;
	}

	
	@Override
	public List<Customer> searchCustomerByStaff(Staff staff) throws Exception {
		staff = entityManager.find(Staff.class, staff.getId());
	    staff.getOwnCustomers().size();
	    entityManager.refresh(staff);
	    return staff.getOwnCustomers();
	}*/
	
	@Override
    public AppUser validateUser(String email, String pwd) {

        AppUser user = null;
        try {
            Query query = entityManager.createQuery("select u from AppUser u where u.email=?1 and u.password=?2");
            query.setParameter(1, email);
            query.setParameter(2, pwd);
            user = (AppUser) query.getSingleResult();

            return user;

        } catch (Exception e) {
            return user;
        }

    }

    @Override
    public List<AppUser> getAllUsers() {
        return entityManager.createNamedQuery(AppUser.GET_ALL_USERS).getResultList();
    }

    @Override
    public void deleteUserById(int id) {
        entityManager.remove(entityManager.find(AppUser.class, id));
    }

    @Override
    public Staff selectStaffById(int id) {
        return entityManager.find(Staff.class, id);
    }

    @Override
    public Staff selectStaffByEmail(String email) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery cQuery = builder.createQuery(Staff.class);
        Root<Staff> s = cQuery.from(Staff.class);
        cQuery.select(s);
        javax.persistence.criteria.Predicate predicate = builder.equal(s.get("email").as(String.class), email);
        cQuery.where(predicate);
        TypedQuery tQuery = entityManager.createQuery(cQuery);

        return (Staff) tQuery.getSingleResult();
    }

    @Override
    public Admin selectAdminById(int id) {
        return entityManager.find(Admin.class, id);
    }

    @Override
    public void updateStaff(Staff s) {
        entityManager.merge(s);
    }

    @Override
    public void updateAdmin(Admin a) {
        entityManager.merge(a);
    }

    @Override
    public void addStaff(Staff s) {
        Query query = entityManager.createNamedQuery(AppUser.GET_LAST_USER);
        query.setMaxResults(1);
        AppUser lastUser = (AppUser) query.getSingleResult();
        s.setId(lastUser.getId() + 1);
        entityManager.persist(s);
    }

    @Override
    public void addAdmin(Admin a) {
        Query query = entityManager.createNamedQuery(AppUser.GET_LAST_USER);
        query.setMaxResults(1);
        AppUser lastUser = (AppUser) query.getSingleResult();
        a.setId(lastUser.getId() + 1);
        entityManager.persist(a);
    }
	

}

