package ass.tutex.repository;

import java.util.List;

import javax.ejb.Remote;

import ass.tutex.repository.entities.AppUser;
import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Customer;
import ass.tutex.repository.entities.Staff;

@Remote
public interface CustomerRepository {
	
    public void addCustomer(Customer customer);

    public List<Customer> getAllCustomers();
    
    public void editCustomer(Customer customer);
    
    public void removeCustomer(int customerId);
    
    public Customer searchCustomerId(int id);
    
	
	public List<Customer> searchCustomerByStaff(Staff staff);
	
	
	//public Customer searchCustomerByContact(Contact contact) throws Exception;
	
	public List<Contact> getAllContactByCustomer(Customer customer);
	
}

