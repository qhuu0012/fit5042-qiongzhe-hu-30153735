package ass.tutex.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;



import javax.inject.Named;

import ass.tutex.repository.ContactRepository;
import ass.tutex.repository.entities.Contact;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;


@Named(value = "contactApplication")
@ApplicationScoped
public class ContactApplication {
	@ManagedProperty(value = "#{customerRepo}")
    ContactRepository contactRepo;
    private ArrayList<Contact> contacts;
    
   
    
   
    private boolean showForm = true;
    
    public boolean isShowForm() {
        return showForm;
    }
    
 // Add some customer and customerContact data from db to app 
    public ContactApplication() throws Exception {
       
        contacts = new ArrayList<>();
        
        
        
        //instantiate customerId
       
        // Assign customer based on the id provided 
        
      //instantiate customerContactManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactRepo = (ContactRepository) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactRepo");

        //get customerContacts from db 
        updateContactList();
      
        
    }
    
    public ArrayList<Contact> getContacts() {
        return contacts;
    }
    
    private void setContacts(ArrayList<Contact> newContacts) {
        this.contacts = newContacts;
    }
    
    public void updateContactList() {
        if (contacts != null && contacts.size() > 0)
        {
            
        }
        else
        {
        	contacts.clear();
        	
        	List<Contact> contacts = contactRepo.getAllContacts();

            for (Contact contact : contacts)
            {
            	contacts.add(contact);
            }

            setContacts((ArrayList<Contact>) contacts);
        }
    }
    
    public void searchContactById(int contactId)
    {
    	contacts.clear();
        
    	contacts.add(contactRepo.searchContactId(contactId));
    }
    
    public void searchContactByCustomerId(int customerId) {
    	contacts.clear();
        List<Contact> contactsByCustomer = contactRepo.searchContactByCustomer(customerId);
        for (Contact contacteach: contactsByCustomer) {
        	contacts.add(contacteach);
        }
    }
    
    public void searchAll()
    {
        contacts.clear();
        
        for (ass.tutex.repository.entities.Contact contact : contactRepo.getAllContacts())
        {
        	contacts.add(contact);
        }
        
        setContacts(contacts);
    }

	
	

}
