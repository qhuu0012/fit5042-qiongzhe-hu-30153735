package ass.tutex.controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;




import ass.tutex.repository.entities.Contact;
import ass.tutex.repository.entities.Industry;
import ass.tutex.repository.entities.Staff;

@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable{
	
    
	
    private int customerId;

	private String address;
	private Industry industry;
	private int noOfStaff;
	private String businessName;
    private String ceo;
    private String officePhone;
    private String email;

    
    private List<Staff> relatedStaffs;
    
    
    private List<Contact> ownContacts;
    
    private String industryType;
    private String industryDesc;
   
    private Set<ass.tutex.repository.entities.Customer> customers;
    
    public Customer(){
    	
    }
    
    
    

	

	public Customer(int customerId, String address, Industry industry, int noOfStaff, String businessName, String ceo,
			String officePhone, String email, List<Staff> relatedStaffs, List<Contact> ownContacts, String industryType,
			String industryDesc, Set<ass.tutex.repository.entities.Customer> customers) {
		super();
		this.customerId = customerId;
		this.address = address;
		this.industry = industry;
		this.noOfStaff = noOfStaff;
		this.businessName = businessName;
		this.ceo = ceo;
		this.officePhone = officePhone;
		this.email = email;
		this.relatedStaffs = relatedStaffs;
		this.ownContacts = ownContacts;
		this.industryType = industryType;
		this.industryDesc = industryDesc;
		this.customers = customers;
	}





	

	public List<Staff> getRelatedStaffs() {
		return relatedStaffs;
	}






	public void setRelatedStaffs(List<Staff> relatedStaffs) {
		this.relatedStaffs = relatedStaffs;
	}






	public String getIndustryType() {
		return industryType;
	}






	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}






	public String getIndustryDesc() {
		return industryDesc;
	}






	public void setIndustryDesc(String industryDesc) {
		this.industryDesc = industryDesc;
	}






	public Set<ass.tutex.repository.entities.Customer> getCustomers() {
		return customers;
	}






	public void setCustomers(Set<ass.tutex.repository.entities.Customer> customers) {
		this.customers = customers;
	}






	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public int getNoOfStaff() {
		return noOfStaff;
	}

	public void setNoOfStaff(int noOfStaff) {
		this.noOfStaff = noOfStaff;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Contact> getOwnContacts() {
		return ownContacts;
	}

	public void setOwnContacts(List<Contact> ownContacts) {
		this.ownContacts = ownContacts;
	}






	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", address=" + address + ", industry=" + industry + ", noOfStaff="
				+ noOfStaff + ", businessName=" + businessName + ", ceo=" + ceo + ", officePhone=" + officePhone
				+ ", email=" + email + ", relatedStaffs=" + relatedStaffs + ", ownContacts=" + ownContacts
				+ ", industryType=" + industryType + ", industryDesc=" + industryDesc + ", customers=" + customers
				+ "]";
	}
	
	

}
