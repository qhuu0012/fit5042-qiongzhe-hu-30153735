package ass.tutex.controllers;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import ass.tutex.repository.AppUserRepository;

@Named(value = "deleteUser")
@RequestScoped
public class DeleteUser {

    @EJB
    private AppUserRepository userRepo;

    /**
     * Creates a new instance of DeleteUser
     */
    public DeleteUser() {
    }


    public String deleteUserById(int userId) {

        userRepo.deleteUserById(userId);

        return "manageUser";
    }
}
